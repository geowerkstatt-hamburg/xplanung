# Mitarbeit 

## Fehler melden

Wenn Sie Fehler in den Regeln finden, dann können Sie unter [Issues > Create Issue](https://bitbucket.org/geowerkstatt-hamburg/xplanung/issues/new) einen Fehlerreport erstellen. Eine Übersicht aller bereits erstellten Fehlerberichte finden Sie ebenfalls unter [Issues](https://bitbucket.org/geowerkstatt-hamburg/xplanung/issues).

## Änderungen 

Wenn Sie Fehler in den Regeln entdecken, dann stellen Sie bitte Ihre Änderungen als [Pull Request](https://bitbucket.org/geowerkstatt-hamburg/xplanung/pull-requests/) zur Verfügung. 
