(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
(
  every $h in //BP_NebenanlagenFlaeche satisfies
  (: Wenn nur eine Zweckbestimmung spezifiziert werden soll, muss dafür immer das Attribut zweckbestimmung verwendet werden. :)
  (
    if (
      exists($h/weitereZweckbestimmung1) or
      exists($h/weitereZweckbestimmung2) or
      exists($h/weitereZweckbestimmung3) or
      exists($h/weitereZweckbestimmung4))
    then (
      exists($h/zweckbestimmung)
    )
    else true()
  )
    and
    (: Wenn zur Spezifikation mehrerer Zweckbestimmungen das Attribut zweckbestimmung mehrfach verwendet wird, dürfen die Attribute weitereZweckbestimmung i (i = 1, 2, 3, 4) nicht verwendet werden. :)
  (
    if ( count($h/zweckbestimmung) > 1)
    then (
      not(exists($h/weitereZweckbestimmung1)) and
      not(exists($h/weitereZweckbestimmung2)) and
      not(exists($h/weitereZweckbestimmung3)) and
      not(exists($h/weitereZweckbestimmung4))
    )
    else true()
  )
    and
    (: Wenn mehr als eine Zweckbestimmung durch unterschiedliche Attribute spezifiziert werden sollen, sind die Attribute weitereZweckbestimmung i (i = 1, 2, 3, 4) in aufsteigender Reihenfolge zu belegen. :)
  (
    if (
      exists($h/weitereZweckbestimmung1) and
      exists($h/weitereZweckbestimmung2) and
      exists($h/weitereZweckbestimmung3) and
      exists($h/weitereZweckbestimmung4)
    )
    then (
      true()
    )
    else if (
      exists($h/weitereZweckbestimmung1) and
      exists($h/weitereZweckbestimmung2) and
      exists($h/weitereZweckbestimmung3)
    )
    then (
        true()
      )
    else if (
        exists($h/weitereZweckbestimmung1) and
        exists($h/weitereZweckbestimmung2)
      )
      then (
          true()
        )
      else if (
          exists($h/weitereZweckbestimmung1)
        )
        then (
            true()
          )
        else if (
            not(exists($h/weitereZweckbestimmung1)) and
            not(exists($h/weitereZweckbestimmung2)) and
            not(exists($h/weitereZweckbestimmung3)) and
            not(exists($h/weitereZweckbestimmung4))
          )
          then (
              true()
            )
          else false()
  )
)
