(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
declare namespace xplan='http://www.xplanung.de/xplangml/4/1';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

(
  every $h in //BP_GemeinschaftsanlagenFlaeche satisfies
  (
    if (
      exists($h/detaillierteZweckbestimmung[1]) and
      not(exists($h/detaillierteZweckbestimmung[2]))
    )
    then (
      not(exists($h/weitereDetailZweckbestimmung1)) and
      not(exists($h/weitereDetailZweckbestimmung2)) and
      not(exists($h/weitereDetailZweckbestimmung3)) and
      not(exists($h/weitereDetailZweckbestimmung4))
    )
    else boolean('false')
  )
)
and
(
  every $h in //BP_GemeinschaftsanlagenFlaeche[detaillierteZweckbestimmung[2]] satisfies
  not(exists($h/weitereDetailZweckbestimmung1)) and
  not(exists($h/weitereDetailZweckbestimmung2)) and
  not(exists($h/weitereDetailZweckbestimmung3)) and
  not(exists($h/weitereDetailZweckbestimmung4))
)
and
(
  every $h in //BP_GemeinschaftsanlagenFlaeche satisfies
  (
    if (
      exists($h/weitereDetailZweckbestimmung1) and
      exists($h/weitereDetailZweckbestimmung2) and
      exists($h/weitereDetailZweckbestimmung3) and
      exists($h/weitereDetailZweckbestimmung4)
    )
    then (
      boolean('true')
    )
    else if (
      exists($h/weitereDetailZweckbestimmung1) and
      exists($h/weitereDetailZweckbestimmung2) and
      exists($h/weitereDetailZweckbestimmung3)
    )
    then (
      boolean('true')
    )
    else if (
      exists($h/weitereDetailZweckbestimmung1) and
      exists($h/weitereDetailZweckbestimmung2)
    )
    then (
      boolean('true')
    )
    else if (
      exists($h/weitereDetailZweckbestimmung1)
    )
    then (
      boolean('true')
    )
    else boolean('false')
  )
)
