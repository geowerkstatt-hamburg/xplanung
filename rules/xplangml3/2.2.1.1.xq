(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/3/0';
declare namespace gml='http://www.opengis.net/gml';

for $h in //*[matches(name(), 'FP_Flaechenschlussobjekt|FP_Ueberlagerungsobjekt|FP_BebauungsFlaeche|FP_KeineZentrAbwasserBeseitigungFlaeche|FP_Erhaltungssatzung|FP_DenkmalschutzEnsemble|FP_Grabungsschutzgebiet|FP_Gemeinbedarf|FP_SpielSportanlage|FP_WaldFlaeche|FP_LandwirtschaftsFlaeche|FP_Gruen|FP_SchutzPflegeEntwicklung|FP_Schutzgebiet|FP_AusgleichsFlaeche|FP_GenerischesObjekt|FP_Kennzeichnung|FP_PriviligiertesVorhaben|FP_Fachgesetz|FP_VorbehalteFlaeche|FP_UnverbindlicheVormerkung|FP_VerEntsorgung|FP_Luftverkehr|FP_Bahnverkehr|FP_Strassenverkehr|FP_Gewaesser|FP_Wasserwirtschaft|FP_Wasserrecht|FP_VorbHochwSchutz|FP_AufschuettungsFlaeche|FP_AbgrabungsFlaeche|FP_BodenschaetzeFlaeche|BP_BaugebietsTeilFlaeche|BP_BesondererNutzungszweckFlaeche|BP_GemeinbedarfsFlaeche|BP_SpielSportanlagenFlaeche|BP_VerEntsorgung|BP_StrassenVerkehrsFlaeche|BP_VerkehrsflaecheBesondererZweckbestimmung|BP_LuftverkehrFlaeche|BP_BahnVerkehr|BP_Strassenkoerper|BP_GewaesserFlaeche|BP_WasserwirtschaftsFlaeche|BP_WasserrechtlicheFestsetzungsFlaeche|BP_VorbHochwSchutzFlaeche|BP_SchutzPflegeEntwicklungsFlaeche|BP_AnpflanzungBindungErhaltung|BP_Schutzgebiet|BP_AusgleichsMassnahme|BP_SchutzPflegeEntwicklungsMassnahme|BP_AusgleichsFlaeche|BP_LandwirtschaftsFlaeche|BP_WaldFlaeche|BP_GruenFlaeche|BP_KennzeichnungsFlaeche|BP_Wegerecht|BP_GenerischesObjekt|BP_Fachgesetz|BP_UnverbindlicheVormerkung|BP_AufschuettungsFlaeche|BP_AbgrabungsFlaeche|BP_BodenschaetzeFlaeche|BP_RekultivierungsFlaeche|BP_Immissionsschutz|BP_Ueberlagerungsobjekt|BP_Flaechenschlussobjekt') and position]
where (
    $h/position/gml:Polygon or
    $h/position/gml:MultiSurface or
    $h/position/gml:LinearRing or
    $h/position/gml:PolygonPatch or
    $h/position/gml:Ring
)
and not (
    $h/flaechenschluss
)
return $h/@gml:id/string()
