(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //BP_BaugebietsTeilFlaeche/besondereArtDerBaulNutzung
where not (
	(
		(
			$h/text() = '1000' or 
			$h/text() = '1100' or 
			$h/text() = '1200' or 
			$h/text() = '1300'
		)
		and (
			not ($h/../allgArtDerBaulNutzung) or
			$h/../allgArtDerBaulNutzung/text() = '1000'
		)
	)
	or
	(
		(
			$h/text() = '1400' or
			$h/text() = '1450' or
			$h/text() = '1500' or 
			$h/text() = '1550' or 
			$h/text() = '1600'
		)
		and (
			not ($h/../allgArtDerBaulNutzung) or
			$h/../allgArtDerBaulNutzung/text() = '2000'
		)
	)
	or
	(
		(
			$h/text() = '1700' or 
			$h/text() = '1800' 
		)
		and (
			not ($h/../allgArtDerBaulNutzung) or
			$h/../allgArtDerBaulNutzung/text() = '3000'
		)
	)	
	or
	(
		(
			$h/text() = '2000' or 
			$h/text() = '2100' or 
			$h/text() = '3000' or 
			$h/text() = '4000'
		)
		and (
			not ($h/../allgArtDerBaulNutzung) or
			$h/../allgArtDerBaulNutzung/text() = '4000'
		)
	)  	
	or
	(
		(
			$h/text() = '9999'
		)
	)
)
return $h/../@gml:id/string()