(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(local-name(), 'BP_AbstandsMass|BP_AbweichungVonBaugrenze|BP_BauGrenze|BP_BauLinie|BP_BereichOhneEinAusfahrtLinie|BP_EinfahrtsbereichLinie|BP_GebaeudeStellung|BP_NutzungsartenGrenze|BP_RichtungssektorGrenze|BP_StrassenbegrenzungsLinie')]
where not (
	$h/position/gml:Curve or
	$h/position/gml:LineString or
	$h/position/gml:MultiCurve or
	$h/position/gml:LineStringSegment or
	$h/position/gml:Arc or
	$h/position/gml:Circle
)
return $h/@gml:id/string()
