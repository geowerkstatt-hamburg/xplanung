(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(local-name(), 'BP_AbgrabungsFlaeche|BP_AbstandsFlaeche|BP_AbweichungVonUeberbaubarerGrundstuecksFlaeche|BP_AufschuettungsFlaeche|BP_AusgleichsFlaeche|BP_BaugebietsTeilFlaeche|BP_BesondererNutzungszweckFlaeche|BP_EingriffsBereich|BP_FlaecheOhneFestsetzung|BP_FoerderungsFlaeche|BP_FreiFlaeche|BP_GebaeudeFlaeche|BP_GemeinbedarfsFlaeche|BP_GemeinschaftsanlagenFlaeche|BP_GruenFlaeche|BP_KennzeichnungsFlaeche|BP_KleintierhaltungFlaeche|BP_LandwirtschaftsFlaeche|BP_NebenanlagenAusschlussFlaeche|BP_NebenanlagenFlaeche|BP_NichtUeberbaubareGrundstuecksflaeche|BP_PersGruppenBestimmteFlaeche|BP_RegelungVergnuegungsstaetten|BP_SchutzPflegeEntwicklungsFlaeche|BP_SpezielleBauweise|BP_SpielSportanlagenFlaeche|BP_TechnischeMassnahmenFlaeche|BP_TextAbschnittFlaeche|BP_UeberbaubareGrundstuecksFlaeche|BP_Veraenderungssperre|BP_WaldFlaeche|BP_WohngebaeudeFlaeche|BP_ZentralerVersorgungsbereich|BP_ZusatzkontingentLaermFlaeche')]
where (
  $h/flaechenschluss != 'false'
  and
  $h/ebene != 0
)
return $h/@gml:id/string()
