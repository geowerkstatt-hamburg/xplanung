(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml = 'http://www.opengis.net/gml/3.2';
declare namespace xlink = 'http://www.w3.org/1999/xlink';

for $id in //planinhalt/@xlink:href/string()
where not(
	//BP_AbgrabungsFlaeche[@gml:id eq substring($id,2)] or
	//BP_AbstandsFlaeche[@gml:id eq substring($id,2)] or
	//BP_AbstandsMass[@gml:id eq substring($id,2)] or
	//BP_AbweichungVonBaugrenze[@gml:id eq substring($id,2)] or
	//BP_AbweichungVonUeberbaubarerGrundstuecksFlaeche[@gml:id eq substring($id,2)] or
	//BP_AnpflanzungBindungErhaltung[@gml:id eq substring($id,2)] or
	//BP_AufschuettungsFlaeche[@gml:id eq substring($id,2)] or
	//BP_AusgleichsFlaeche[@gml:id eq substring($id,2)] or
	//BP_AusgleichsMassnahme[@gml:id eq substring($id,2)] or
	//BP_BaugebietsTeilFlaeche[@gml:id eq substring($id,2)] or
	//BP_BauGrenze[@gml:id eq substring($id,2)] or
	//BP_BauLinie[@gml:id eq substring($id,2)] or
	//BP_BereichOhneEinAusfahrtLinie[@gml:id eq substring($id,2)] or
	//BP_BesondererNutzungszweckFlaeche[@gml:id eq substring($id,2)] or
	//BP_EinfahrtPunkt[@gml:id eq substring($id,2)] or
	//BP_EinfahrtsbereichLinie[@gml:id eq substring($id,2)] or
	//BP_EingriffsBereich[@gml:id eq substring($id,2)] or
	//BP_FestsetzungNachLandesrecht[@gml:id eq substring($id,2)] or
	//BP_Flaechenschlussobjekt[@gml:id eq substring($id,2)] or
	//BP_FlaecheOhneFestsetzung[@gml:id eq substring($id,2)] or
	//BP_FoerderungsFlaeche[@gml:id eq substring($id,2)] or
	//BP_FreiFlaeche[@gml:id eq substring($id,2)] or
	//BP_GebaeudeFlaeche[@gml:id eq substring($id,2)] or
	//BP_GebaeudeStellung[@gml:id eq substring($id,2)] or
	//BP_GemeinbedarfsFlaeche[@gml:id eq substring($id,2)] or
	//BP_GemeinschaftsanlagenFlaeche[@gml:id eq substring($id,2)] or
	//BP_GemeinschaftsanlagenZuordnung[@gml:id eq substring($id,2)] or
	//BP_GenerischesObjekt[@gml:id eq substring($id,2)] or
	//BP_GruenFlaeche[@gml:id eq substring($id,2)] or
	//BP_HoehenMass[@gml:id eq substring($id,2)] or
	//BP_Immissionsschutz[@gml:id eq substring($id,2)] or
	//BP_KennzeichnungsFlaeche[@gml:id eq substring($id,2)] or
	//BP_KleintierhaltungFlaeche[@gml:id eq substring($id,2)] or
	//BP_LandwirtschaftsFlaeche[@gml:id eq substring($id,2)] or
	//BP_NebenanlagenAusschlussFlaeche[@gml:id eq substring($id,2)] or
	//BP_NebenanlagenFlaeche[@gml:id eq substring($id,2)] or
	//BP_NichtUeberbaubareGrundstuecksflaeche[@gml:id eq substring($id,2)] or
	//BP_NutzungsartenGrenze[@gml:id eq substring($id,2)] or
	//BP_PersGruppenBestimmteFlaeche[@gml:id eq substring($id,2)] or
	//BP_RegelungVergnuegungsstaetten[@gml:id eq substring($id,2)] or
	//BP_RichtungssektorGrenze[@gml:id eq substring($id,2)] or
	//BP_SchutzPflegeEntwicklungsFlaeche[@gml:id eq substring($id,2)] or
	//BP_SchutzPflegeEntwicklungsMassnahme[@gml:id eq substring($id,2)] or
	//BP_SpezielleBauweise[@gml:id eq substring($id,2)] or
	//BP_SpielSportanlagenFlaeche[@gml:id eq substring($id,2)] or
	//BP_StrassenbegrenzungsLinie[@gml:id eq substring($id,2)] or
	//BP_Strassenkoerper[@gml:id eq substring($id,2)] or
	//BP_TechnischeMassnahmenFlaeche[@gml:id eq substring($id,2)] or
	//BP_TextAbschnittFlaeche[@gml:id eq substring($id,2)] or
	//BP_UeberbaubareGrundstuecksFlaeche[@gml:id eq substring($id,2)] or
	//BP_UnverbindlicheVormerkung[@gml:id eq substring($id,2)] or
	//BP_Veraenderungssperre[@gml:id eq substring($id,2)] or
	//BP_VerEntsorgung[@gml:id eq substring($id,2)] or
	//BP_WaldFlaeche[@gml:id eq substring($id,2)] or
	//BP_Wegerecht[@gml:id eq substring($id,2)] or
	//BP_WohngebaeudeFlaeche[@gml:id eq substring($id,2)] or
	//BP_ZentralerVersorgungsbereich[@gml:id eq substring($id,2)] or
	//BP_ZusatzkontingentLaerm[@gml:id eq substring($id,2)] or
	//BP_ZusatzkontingentLaermFlaeche[@gml:id eq substring($id,2)] or
	//FP_Abgrabung[@gml:id eq substring($id,2)] or
	//FP_AnpassungKlimawandel[@gml:id eq substring($id,2)] or
	//FP_Aufschuettung[@gml:id eq substring($id,2)] or
	//FP_AusgleichsFlaeche[@gml:id eq substring($id,2)] or
	//FP_BebauungsFlaeche[@gml:id eq substring($id,2)] or
	//FP_DarstellungNachLandesrecht[@gml:id eq substring($id,2)] or
	//FP_Flaechenschlussobjekt[@gml:id eq substring($id,2)] or
	//FP_FlaecheOhneDarstellung[@gml:id eq substring($id,2)] or
	//FP_Gemeinbedarf[@gml:id eq substring($id,2)] or
	//FP_GenerischesObjekt[@gml:id eq substring($id,2)] or
	//FP_Gruen[@gml:id eq substring($id,2)] or
	//FP_KeineZentrAbwasserBeseitigungFlaeche[@gml:id eq substring($id,2)] or
	//FP_Kennzeichnung[@gml:id eq substring($id,2)] or
	//FP_Landwirtschaft[@gml:id eq substring($id,2)] or
	//FP_Nutzungsbeschraenkung[@gml:id eq substring($id,2)] or
	//FP_PrivilegiertesVorhaben[@gml:id eq substring($id,2)] or
	//FP_SchutzPflegeEntwicklung[@gml:id eq substring($id,2)] or
	//FP_SpielSportanlage[@gml:id eq substring($id,2)] or
	//FP_TextAbschnittFlaeche[@gml:id eq substring($id,2)] or
	//FP_UnverbindlicheVormerkung[@gml:id eq substring($id,2)] or
	//FP_VerEntsorgung[@gml:id eq substring($id,2)] or
	//FP_VorbehalteFlaeche[@gml:id eq substring($id,2)] or
	//FP_WaldFlaeche[@gml:id eq substring($id,2)] or
	//FP_ZentralerVersorgungsbereich[@gml:id eq substring($id,2)] or
	//LP_BiotopverbundBiotopvernetzung[@gml:id eq substring($id,2)] or
	//LP_Eingriffsregelung[@gml:id eq substring($id,2)] or
	//LP_GenerischesObjekt[@gml:id eq substring($id,2)] or
	//LP_SchutzBestimmterTeileVonNaturUndLandschaft[@gml:id eq substring($id,2)] or
	//LP_TextAbschnittObjekt[@gml:id eq substring($id,2)] or
	//LP_ZieleErfordernisseMassnahmen[@gml:id eq substring($id,2)] or
	//RP_Achse[@gml:id eq substring($id,2)] or
	//RP_Bodenschutz[@gml:id eq substring($id,2)] or
	//RP_Einzelhandel[@gml:id eq substring($id,2)] or
	//RP_Energieversorgung[@gml:id eq substring($id,2)] or
	//RP_Entsorgung[@gml:id eq substring($id,2)] or
	//RP_Erholung[@gml:id eq substring($id,2)] or
	//RP_ErneuerbareEnergie[@gml:id eq substring($id,2)] or
	//RP_Forstwirtschaft[@gml:id eq substring($id,2)] or
	//RP_Freiraum[@gml:id eq substring($id,2)] or
	//RP_Funktionszuweisung[@gml:id eq substring($id,2)] or
	//RP_GenerischesObjekt[@gml:id eq substring($id,2)] or
	//RP_Gewaesser[@gml:id eq substring($id,2)] or
	//RP_Grenze[@gml:id eq substring($id,2)] or
	//RP_GruenzugGruenzaesur[@gml:id eq substring($id,2)] or
	//RP_Hochwasserschutz[@gml:id eq substring($id,2)] or
	//RP_IndustrieGewerbe[@gml:id eq substring($id,2)] or
	//RP_Klimaschutz[@gml:id eq substring($id,2)] or
	//RP_Kommunikation[@gml:id eq substring($id,2)] or
	//RP_Kulturlandschaft[@gml:id eq substring($id,2)] or
	//RP_LaermschutzBauschutz[@gml:id eq substring($id,2)] or
	//RP_Landwirtschaft[@gml:id eq substring($id,2)] or
	//RP_Luftverkehr[@gml:id eq substring($id,2)] or
	//RP_NaturLandschaft[@gml:id eq substring($id,2)] or
	//RP_NaturschutzrechtlichesSchutzgebiet[@gml:id eq substring($id,2)] or
	//RP_Planungsraum[@gml:id eq substring($id,2)] or
	//RP_RadwegWanderweg[@gml:id eq substring($id,2)] or
	//RP_Raumkategorie[@gml:id eq substring($id,2)] or
	//RP_Rohstoff[@gml:id eq substring($id,2)] or
	//RP_Schienenverkehr[@gml:id eq substring($id,2)] or
	//RP_Siedlung[@gml:id eq substring($id,2)] or
	//RP_SonstigeInfrastruktur[@gml:id eq substring($id,2)] or
	//RP_SonstigerFreiraumschutz[@gml:id eq substring($id,2)] or
	//RP_SonstigerSiedlungsbereich[@gml:id eq substring($id,2)] or
	//RP_SonstVerkehr[@gml:id eq substring($id,2)] or
	//RP_SozialeInfrastruktur[@gml:id eq substring($id,2)] or
	//RP_Sperrgebiet[@gml:id eq substring($id,2)] or
	//RP_Sportanlage[@gml:id eq substring($id,2)] or
	//RP_Strassenverkehr[@gml:id eq substring($id,2)] or
	//RP_Verkehr[@gml:id eq substring($id,2)] or
	//RP_Wasserschutz[@gml:id eq substring($id,2)] or
	//RP_Wasserverkehr[@gml:id eq substring($id,2)] or
	//RP_Wasserwirtschaft[@gml:id eq substring($id,2)] or
	//RP_WohnenSiedlung[@gml:id eq substring($id,2)] or
	//RP_ZentralerOrt[@gml:id eq substring($id,2)] or
	//SO_Baubeschraenkung[@gml:id eq substring($id,2)] or
	//SO_Bodenschutzrecht[@gml:id eq substring($id,2)] or
	//SO_Denkmalschutzrecht[@gml:id eq substring($id,2)] or
	//SO_Forstrecht[@gml:id eq substring($id,2)] or
	//SO_Gebiet[@gml:id eq substring($id,2)] or
	//SO_Gelaendemorphologie[@gml:id eq substring($id,2)] or
	//SO_Gewaesser[@gml:id eq substring($id,2)] or
	//SO_Grenze[@gml:id eq substring($id,2)] or
	//SO_Luftverkehrsrecht[@gml:id eq substring($id,2)] or
	//SO_Objekt[@gml:id eq substring($id,2)] or
	//SO_Schienenverkehrsrecht[@gml:id eq substring($id,2)] or
	//SO_SchutzgebietWasserrecht[@gml:id eq substring($id,2)] or
	//SO_Sichtflaeche[@gml:id eq substring($id,2)] or
	//SO_SonstigesRecht[@gml:id eq substring($id,2)] or
	//SO_Strassenverkehr[@gml:id eq substring($id,2)] or
	//SO_TextAbschnittFlaeche[@gml:id eq substring($id,2)] or
	//SO_Wasserrecht[@gml:id eq substring($id,2)] or
	//SO_Wasserwirtschaft[@gml:id eq substring($id,2)]
)
return substring($id,2)
