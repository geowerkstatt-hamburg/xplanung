(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/3';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //@uom
where not (
  $h = "m" or $h = "urn:adv:uom:m" or
  $h = "m2" or $h = "urn:adv:uom:m2" or 
  $h = "m3" or $h = "urn:adv:uom:m3" or 
  $h = "grad" or $h = "urn:adv:uom:grad" or
  $h = "db" or
  $h = "km/h" or
  $h = "m/sec"
)
return $h/ancestor::gml:featureMember/*[1]/@gml:id/string()
