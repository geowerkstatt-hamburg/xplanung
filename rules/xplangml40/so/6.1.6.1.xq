(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/0';
declare namespace xplan='http://www.xplanung.de/xplangml/4/0';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

every $h in //SO_Linienobjekt[position] satisfies
(
  exists($h/position/gml:Curve) and 
  exists($h/position/gml:Curve/descendant-or-self::gml:segments) and
  not(exists($h/position/gml:Curve/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:Curve/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:Curve/descendant-or-self::gml:posList)) and
  not(exists($h/position/gml:Curve/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:Curve/descendant-or-self::gml:pos))
)
or
(
  exists($h/position/gml:LineString) and 
  exists($h/position/gml:LineString/descendant-or-self::gml:posList) and
  not(exists($h/position/gml:LineString/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:LineString/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:LineString/descendant-or-self::gml:segments)) and
  not(exists($h/position/gml:LineString/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:LineString/descendant-or-self::gml:pos))
)
or
(
  exists($h/position/gml:MultiCurve) and 
  exists($h/position/gml:MultiCurve/descendant-or-self::gml:segments) and
  not(exists($h/position/gml:MultiCurve/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:MultiCurve/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:MultiCurve/descendant-or-self::gml:posList)) and
  not(exists($h/position/gml:MultiCurve/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:MultiCurve/descendant-or-self::gml:pos))
) 
or
(
  exists($h/position/gml:LineStringSegment) and 
  exists($h/position/gml:LineStringSegment/descendant-or-self::gml:posList) and
  not(exists($h/position/gml:LineStringSegment/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:LineStringSegment/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:LineStringSegment/descendant-or-self::gml:segments)) and
  not(exists($h/position/gml:LineStringSegment/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:LineStringSegment/descendant-or-self::gml:pos))
) 
or
(
  exists($h/position/gml:Arc) and 
  exists($h/position/gml:Arc/descendant-or-self::gml:posList) and
  not(exists($h/position/gml:Arc/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:Arc/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:Arc/descendant-or-self::gml:segments)) and
  not(exists($h/position/gml:Arc/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:Arc/descendant-or-self::gml:pos))
)
or
(
  exists($h/position/gml:Circle) and 
  exists($h/position/gml:Circle/descendant-or-self::gml:posList) and
  not(exists($h/position/gml:Circle/descendant-or-self::gml:arcString)) and
  not(exists($h/position/gml:Circle/descendant-or-self::gml:segment)) and
  not(exists($h/position/gml:Circle/descendant-or-self::gml:segments)) and
  not(exists($h/position/gml:Circle/descendant-or-self::gml:coordinates)) and
  not(exists($h/position/gml:Circle/descendant-or-self::gml:pos))
)
