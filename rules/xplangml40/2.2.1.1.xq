(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/0';
declare namespace gml = 'http://www.opengis.net/gml/3.2';

for $h in //*[matches(name(), 'SO_SchutzgebietWasserrecht|SO_Schienenverkehrsrecht|SO_Forstrecht|SO_SonstigesRecht|SO_Luftverkehrsrecht|SO_Wasserrecht|SO_SchutzgebietNaturschutzrecht|SO_Gebiet|SO_Strassenverkehrsrecht|SO_Denkmalschutzrecht|SO_SchutzgebietSonstigesRecht|SO_Bodenschutzrecht|BP_GemeinschaftsanlagenZuordnung|BP_PersGruppenBestimmteFlaeche|FP_Gemeinbedarf|BP_StrassenVerkehrsFlaeche|BP_KennzeichnungsFlaeche|BP_RekultivierungsFlaeche|FP_AusgleichsFlaeche|BP_Immissionsschutz|BP_GebaeudeFlaeche|BP_GruenFlaeche|FP_UnverbindlicheVormerkung|BP_AbstandsFlaeche|BP_Ueberlagerungsobjekt|BP_TextlicheFestsetzungsFlaeche|BP_Veraenderungssperre|FP_Kennzeichnung|FP_VerEntsorgung|FP_GenerischesObjekt|FP_SpielSportanlage|BP_GemeinschaftsanlagenFlaeche|BP_NebenanlagenAusschlussFlaeche|BP_HoehenMass|BP_AusgleichsMassnahme|BP_AufschuettungsFlaeche|BP_BesondererNutzungszweckFlaeche|FP_TextlicheDarstellungsFlaeche|BP_NebenanlagenFlaeche|BP_Landwirtschaft|BP_SpielSportanlagenFlaeche|BP_Strassenkoerper|BP_AbgrabungsFlaeche|FP_VorbehalteFlaeche|FP_BebauungsFlaeche|FP_PrivilegiertesVorhaben|BP_SchutzPflegeEntwicklungsFlaeche|FP_LandwirtschaftsFlaeche|BP_FoerderungsFlaeche|FP_Ueberlagerungsobjekt|FP_SchutzPflegeEntwicklung|FP_Gruen|FP_Strassenverkehr|BP_UnverbindlicheVormerkung|BP_Schutzgebiet|BP_DenkmalschutzEinzelanlage|BP_LuftreinhalteFlaeche|BP_DenkmalschutzEnsembleFlaeche|BP_VerkehrsflaecheBesondererZweckbestimmung|BP_ErhaltungsBereichFlaeche|BP_SchutzPflegeEntwicklungsMassnahme|BP_GewaesserFlaeche|FP_AbgrabungsFlaeche|BP_AusgleichsFlaeche|BP_UeberbaubareGrundstuecksFlaeche|BP_FreiFlaeche|FP_BodenschaetzeFlaeche|FP_Wasserwirtschaft|BP_GemeinbedarfsFlaeche|BP_ErneuerbareEnergieFlaeche|BP_SpezielleBauweise|BP_VerEntsorgung|BP_GenerischesObjekt|FP_AufschuettungsFlaeche|BP_AbstandsMass|BP_KleintierhaltungFlaeche|BP_EingriffsBereich|FP_KeineZentrAbwasserBeseitigungFlaeche|BP_Wegerecht|BP_BodenschaetzeFlaeche|FP_NutzungsbeschraenkungsFlaeche|FP_Gewaesser|BP_WasserwirtschaftsFlaeche|BP_BaugebietsTeilFlaeche|FP_Flaechenschlussobjekt|BP_AnpflanzungBindungErhaltung|FP_WaldFlaeche|BP_WaldFlaeche') and position]
where (
    $h/position/gml:Polygon or
    $h/position/gml:MultiSurface or
    $h/position/gml:LinearRing or
    $h/position/gml:PolygonPatch or
    $h/position/gml:Ring
)
and not (
    $h/flaechenschluss
)
return $h/@gml:id/string()

