# Wichtiger Hinweis
**Dieses Repository ist umgezogen und wird zukünftig unter https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard weiterentwickelt.**
***
 
 
 
# XPlanung

Diese Repository beinhaltet XPlanung-Validierungsregeln des [XPlanung-Standards](https://xleitstelle.de/xplanung/releases-xplanung) als XQuery-Anweisungen.
Weitere Informationen zum Standard XPlanung finden Sie bei der [Leitstelle XPlanung/XBau](http://xleitstelle.de/). Der öffentlich verfügbare XPlanGML Validator der Leitstelle ist unter der Adresse https://www.xplanungsplattform.de/xplan-validator/ erreichbar.

## Inhalt

Die Implementierung der Konformitätsregeln sind nach Version getrennt unterhalb des Ordners `rules` abgelegt: 

- XPlanung Version 6.0 im Ordner [xplangml60](rules/xplangml60)
- XPlanung Version 5.4 im Ordner [xplangml54](rules/xplangml54)
- XPlanung Version 5.3 im Ordner [xplangml53](rules/xplangml53)
- XPlanung Version 5.2 im Ordner [xplangml52](rules/xplangml52)
- XPlanung Version 5.1 im Ordner [xplangml51](rules/xplangml51)
- XPlanung Version 5.0 im Ordner [xplangml50](rules/xplangml50)
- XPlanung Version 4.1 im Ordner [xplangml41](rules/xplangml41)
- XPlanung Version 4.0 im Ordner [xplangml40](rules/xplangml40)

### Umfang der Implementierung

Aktuell sind die geometrischen Konformitätsbedingungen aus dem Kapitel 2.2 nicht bzw. nur in Teilen als XQuery-Anweisungen umgesetzt. Für die Version 6.0 ist die Regel 4.5.2.4 hinzugekommen, die ebenfalls nicht als XQuery-Anweisungen umgesetzt wurde. 

## Voraussetzungen

Für die Ausführung der XQuery-Anweisungen wird ein [XQuery](https://www.w3.org/XML/Query/) 3.0 kompatibler Parser benötigt.
Eine Übersicht von Werkzeugen ist auf der Seite von [Wikipedia zu XQuery](https://en.wikipedia.org/wiki/XQuery) zu finden.

Entwickelt und getestet wurden die XQuery-Anweisungen mit dem Werkzeug [BaseX](http://basex.org/). 
Eingesetzt werden diese Regeln u.a. im [XPlanValidator](https://www.xplanungsplattform.de/xplan-validator/) der [Leitstelle XPlanung](http://xleitstelle.de/) und der Lösung xPlanBox der [Firma lat/lon](https://www.lat-lon.de).

## Mitarbeit

Regeln für die Mitarbeit finden Sie in [CONTRIBUTING.md](CONTRIBUTING.md).

## Versionierung

Die Versionierung der Regeln folgt dem Versionierungsschema von [SemVer](http://semver.org/).  Eine Übersicht der Versionen ist unter [Tags](https://bitbucket.org/geowerkstatt-hamburg/xplanung/downloads/?tab=tags) zu finden. 

## Autoren

* **lat/lon GmbH** - **Hersteller der xPlanBox** - [lat/lon](https://github.com/lat-lon)

Personen, die an diesem Projekt mitgearbeitet haben, stehen in [contributors](CONTRIBUTORS.md).

## Lizenz

Dieses Projekt ist unter der GNU Lesser General Public License (LGPL), version 2.1 veröffentlicht. Weiter Informationen zur Lizenz stehen in [LICENSE.txt](LICENSE.txt). 


